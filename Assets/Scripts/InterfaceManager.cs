﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InterfaceManager : MonoBehaviour
{
    [SerializeField]
    Slider TimeSpeed;
    [SerializeField]
    Text countDaySpeed;
    [SerializeField]
    GameObject allPlanet;
    float[] startSpeedValue; 

    float timeSpeed;
    public void Start()
    {
        startSpeedValue = new float[allPlanet.transform.childCount];
        for (int i = 0; i < allPlanet.transform.childCount; i++)
        {
            PlanetRotator x = allPlanet.transform.GetChild(i).GetComponent<PlanetRotator>();
            startSpeedValue[i] = x.SpeedRotate;
        }
        
    }


    public void Update()
    {
        timeSpeed = Mathf.Round(TimeSpeed.value);
        countDaySpeed.text = "Количество дней / сек: " + timeSpeed.ToString();

        
        for (int i = 0; i < allPlanet.transform.childCount; i++)
        {        
            PlanetRotator x = allPlanet.transform.GetChild(i).GetComponent<PlanetRotator>();
            if (x.SpeedRotate < 3)
            {
                x.SpeedRotate = startSpeedValue[i] + timeSpeed/100;
            }
            else
                x.SpeedRotate = startSpeedValue[i] + timeSpeed;
        }
        
    }
    
}
