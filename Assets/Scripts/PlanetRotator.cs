﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetRotator : MonoBehaviour
{ 
    public float SpeedRotate;   
    public void Update()
    {
        float angle = SpeedRotate * Time.deltaTime;
        transform.Rotate(Vector3.up, angle);
    }
}
